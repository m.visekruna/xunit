﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using EmployeesApp.Validation;

namespace NUnitTests.Validation
{
    public class AccountNumberValidationTests
    {
        private AccountNumberValidation _sut;

        [SetUp]
        public void SetUp()
        {
            _sut = new AccountNumberValidation();
        }

        [TestCase("AAA-1234567890-AB")]
        public void GivenValidAccountNumber_WhenIsValidIsCalled_ThenTrueIsReturned(string accountNumber)
        {
            var result = _sut.IsValid(accountNumber);
            Assert.That(result, Is.True);
        }

        [Test]
        public void GivenAccountNumberWithInvalidStartingPart_WhenIsValidIsCalled_ThenFalseIsReturned()
        {
            var accountNumber = "AA-1234567890-AB";
            var result = _sut.IsValid(accountNumber);
            Assert.That(result, Is.False);
        }

        [Test]
        public void GivenAccountNumberWithInvalidMiddlePart_WhenIsValidIsCalled_ThenFalseIsReturned()
        {
            var accountNumber = "AAA-123456789-AB";
            var result = _sut.IsValid(accountNumber);
            Assert.That(result, Is.False);
        }

        [Test]
        public void GivenAccountNumberWithInvalidLastPart_WhenIsValidIsCalled_ThenFalseIsReturned()
        {
            var accountNumber = "AAA-1234567890-A";
            var result = _sut.IsValid(accountNumber);
            Assert.That(result, Is.False);
        }

        [Test]
        public void GivenAccountNumberWithFirstDelimiterMissing_WhenIsValidIsCalled_ThenArgumentExceptionIsThrown()
        {
            var accountNumber = "AAA1234567890-AB";
            Assert.That(() => _sut.IsValid(accountNumber), Throws.ArgumentException);
        }

        [Test]
        public void GivenAccountNumberWithBothDelimitersMissing_WhenIsValidIsCalled_ThenArgumentExceptionIsThrown()
        {
            var accountNumber = "AAA1234567890AB";
            Assert.That(() => _sut.IsValid(accountNumber), Throws.ArgumentException);
        }
    }
}
